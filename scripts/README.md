# Scripts

Contains small scripts used in the TPC test beam data processing and analysis.

## List of scripts

- **do_dataset_list.py**: find all the files using a list of run names
- **do_conversion.py**: converts multiple aqs files into root files
