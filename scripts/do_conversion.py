#! /usr/bin/python

# This python script should be run from the bin folder where the converter executable is (because the path to some files are hardcoded there).
# Therefore all the paths should be expressed as from this bin folder or in an absolute way.
# Author: M. Guigue (mguigue@lpnhe.in2p3.fr)
# Date: Jul 27 2019

import argparse
import os
import re
import subprocess


# pathToBin="/sps/t2k/testbeamdata/2019_06/root/v1/soft/bin"
pathToRawData = "/sps/t2k/testbeamdata/2019_06/raw"
rootToRootData = "/sps/t2k/testbeamdata/2019_06/root"
pathToListData = "/sps/t2k/testbeamdata/2019_06/raw/FullDatasetFilesLists.txt"
stopAfterXfile = 1000000

def get_path_to_root_data(version):
    return  rootToRootData + "/v" + str(version) + "/"

def get_day_from_path(inputFile):
    '''
    Extract the day of the file (to save it in the right location later on)
    '''
    filename = os.path.basename(inputFile)
    matchObj = re.match( r'R(.*)-(.*?)-(.*?).aqs', filename, re.M|re.I)
    return str(matchObj.group(1))

def run_conversion(inputFile, outputFolder):
    '''
    Run a subprocess to convert one file
    '''
    return subprocess.run(["./converter", "-i", inputFile, "-o", outputFolder]).returncode

def do_conversions(args):
    '''
    Do all the conversions using a for loop
    '''
    f = open(pathToListData, "r")

    iterator = 1
    for afile in f:
        if "#" in afile:
            continue
        fullFile = os.path.join(pathToRawData, afile.replace("\n", ""))
        outputPath = os.path.join(get_path_to_root_data(args.version), get_day_from_path(fullFile)) + "/"
        if not os.path.exists(outputPath):
            os.makedirs(outputPath)
        returnCode = run_conversion(fullFile, outputPath)
        if returnCode != 0:
            print("Error: returned code {}".format(returnCode))
            exit(1)
        if iterator == stopAfterXfile:
            exit(0)
        iterator += 1


if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Do conversion of raw data file.')
    parser.add_argument('--version', metavar='version', type=int,
                        help='indicate the version/tag of the software (ex: 1)', 
                        required=True)

    args = parser.parse_args()
    do_conversions(args)

