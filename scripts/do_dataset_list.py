#!/usr/bin/python

# This script creates the list of all the data files path from an list of data prefixes and saves it into a file.
# It will extract also all the subfiles e.g. ./2019_06_12/R2019_06_12-10_11_25-001.aqs from the prefix R2019_06_12-10_11_25
# Author: M. Guigue (mguigue@lpnhe.in2p3.fr)
# Date: Jul 27 2019

f=open("DatasetFilesList.txt","r")
lines=f.readlines()
files=[]
for x in lines:
    if "#" in x:
        continue
    files.append(x.replace("\n",""))
f.close()

import os, fnmatch
def find(pattern, path):
    result = []
    for root, _, files in os.walk(path):
        for name in files:
            if fnmatch.fnmatch(name, pattern):
                result.append(os.path.join(root, name))
    return result

f = open("FullDatasetFilesLists.txt", "w")
for afile in files:
    list_found = find("{}*.aqs".format(afile), ".")
    if len(list_found) == 0:
        print("Couldn't find {}".format(afile))
        exit(1)
    list_found.sort()
    for apath in list_found:
        f.write(apath + "\n")
f.close()

