#include "T2KConstants.h"
#include "DAQ.h"

#include <fstream>
#include <string>
#include <iostream>

using namespace std;

void DAQ::loadDAQ()
{
    int det, arc;
    string filename;

    std::cout << "Reading detector2arc.txt" << std::endl;
    filename = "../src/DAQ/detector2arc.txt";
    ifstream det2arc(filename.c_str());
    if (!det2arc.good()) {
        cerr << "Error. DAQ.cxx: File not found: " << filename << endl;
        cerr << "Please run converter from the bin folder" << endl;
        exit(1);
    }

    while (!det2arc.eof())
    {
        det2arc >> det >> arc >> ws;
        detector2arc[det] = arc;
        arc2detector[arc] = det;
    }
    det2arc.close();

    int daq;
    std::cout << "Reading arc2daq.txt" << std::endl;
    filename = "../src/DAQ/arc2daq.txt";
    ifstream farc2daq(filename.c_str());
    if (!farc2daq.good()) {
        cerr << "Error. DAQ.cxx: File not found: " << filename << endl;
        cerr << "Please run converter from the bin folder" << endl;
        exit(1);
    }

    while (!farc2daq.eof())
    {
        farc2daq >> arc >> daq >> ws;
        arc2daq[arc] = daq;
        daq2arc[daq] = arc;
    }
    farc2daq.close();
}
