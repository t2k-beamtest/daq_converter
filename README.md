# DAQ converter for the DESY beamtest files

## Installation
Setup the environment. In case of working at LXPLUS do not need to modify the file
```bash
source setup.sh
```
Compile
```bash
cd converter
make
```

## Running
Call the binaries from the bin folder only!
```bash
cd ./bin/
./converter -i input_file -o output_path
```