/*******************************************************************************
             PandaX-III / T2K-II
             ___________________

File:        fdecoder.c

Description: This program decodes the binary data files recorded by the TDCM.


Author:      D. Calvet,        denis.calvetATcea.fr


History:
April 2019    : created from mreader.c

*******************************************************************************/
#include <stdio.h>
#include <string.h>
#include <fcntl.h>
#include <fstream>

#include "../src/fdecoder.h"
#include "../src/datum_decoder.h"
#include "../src/platform_spec.h"
#include "../src/frame.h"

// Marion's classes
#include "../src/Mapping.h"
#include "../src/DAQ.h"
#include "../src/Pads.h"
#include "../src/Pixel.h"
#include "../src/T2KConstants.h"

// ROOT
#include "TH1D.h"
#include "TH2F.h"
#include "TStyle.h"
#include "TCanvas.h"
#include "TFile.h"
#include "TTree.h"
#include "TBranch.h"
#include "TH3.h"

typedef struct _Param
{
  char* inp_file;
  char* out_path;
  FILE *fsrc;
  int has_no_run;
  int show_run;
  unsigned int vflag;
  int sample_index_offset_zs;
  bool test = false;
} Param;

/*******************************************************************************
Global Variables
*******************************************************************************/
Param param;
Features fea;
DatumContext dc;
int verbose;
TH1I *hADCvsTIME[n::pads];
std::vector<long int> eventPos;
int firstEv;
Pixel P;

/**********************************************************************
Useful functions
**********************************************************************/
void scan()
{
  // Reset eventPos vector
  eventPos.clear();

  // Scan the file
  DatumContext_Init(&dc, param.sample_index_offset_zs);
  unsigned short datum;
  int err;
  bool done = true;
  int prevEvnum = -1;
  firstEv = -1;
  int evnum;
  while (done) {
    // Read one short word
    if (fread(&datum, sizeof(unsigned short), 1, param.fsrc) != 1) {
      done = false;
    }
    else {
      fea.tot_file_rd += sizeof(unsigned short);
      // Interpret datum
      if ((err = Datum_Decode(&dc, datum)) < 0) {
        printf("%d Datum_Decode: %s\n", err, &dc.ErrorString[0]);
        done = true;
      }
      else {
        if (dc.isItemComplete) {
          if (dc.ItemType == IT_START_OF_EVENT) {
            //printf("Type : 0x%x \n", dc.ItemType);
            evnum = (int)dc.EventNumber;
            if (firstEv < 0) {
              printf("Is first event type : 0x%x  and number %u \n", dc.ItemType, dc.EventNumber);
              eventPos.push_back(fea.tot_file_rd - sizeof(unsigned short));
              //cout << dc.EventNumber << endl;
              firstEv = evnum;
              prevEvnum = evnum;
            }
            else if (evnum != prevEvnum) {
              eventPos.push_back(fea.tot_file_rd - sizeof(unsigned short));
              prevEvnum = evnum;
            }
          }
          else if (dc.ItemType == IT_ADC_SAMPLE) {}
          else if (dc.ItemType == IT_DATA_FRAME) {}
          else if (dc.ItemType == IT_END_OF_FRAME) {}
          else if (dc.ItemType == IT_MONITORING_FRAME) {}
          else if (dc.ItemType == IT_CONFIGURATION_FRAME) {}
          else if (dc.ItemType == IT_SHORT_MESSAGE) {}
          else if (dc.ItemType == IT_LONG_MESSAGE) {}
          else if (dc.ItemType == IT_TIME_BIN_INDEX) {}
          else if (dc.ItemType == IT_CHANNEL_HIT_HEADER) {}
          else if (dc.ItemType == IT_DATA_FRAME) {}
          else if (dc.ItemType == IT_NULL_DATUM) {}
          else if (dc.ItemType == IT_CHANNEL_HIT_COUNT) {}
          else if (dc.ItemType == IT_LAST_CELL_READ) {}
          else if (dc.ItemType == IT_END_OF_EVENT) {}
          else if (dc.ItemType == IT_PED_HISTO_MD) {}
          else if (dc.ItemType == IT_UNKNOWN) {}
          else if (dc.ItemType == IT_CHAN_PED_CORRECTION) {} //printf("Type : 0x%x \n", dc.ItemType);}
          else if (dc.ItemType == IT_CHAN_ZERO_SUPPRESS_THRESHOLD) {} //printf("Type : 0x%x \n", dc.ItemType);}
          else
          {
            //printf("converter.cxx: Unknow Item Type : 0x%04x\n", dc.ItemType);
            cerr << "converter.cxx: Unknow Item Type : " << dc.ItemType << endl;
            cerr << "Program will exit to prevent the storing of the corrupted data" << endl;
            exit(1);
          }
        }
      }
    }
  }
  cout << "First event : " << firstEv << endl;
  cout << eventPos.size() << " events in the file..." << endl;
}

int padNum(const int &i, const int &j) { return (j * geom::nPadx + i); }
int iFrompad(const int &padnum) { return (padnum % geom::nPadx); }
int jFrompad(const int &padnum) { return (padnum / geom::nPadx); }

void histoEventInit()
{
  gStyle->SetOptStat(0);

  for (int k = 0; k < n::pads; k++)
  {
    char histName[40];
    sprintf(histName, "hADCvsTIME %d", k);
    hADCvsTIME[k] = new TH1I(histName, histName, n::samples, 0, n::samples);
    hADCvsTIME[k]->SetMinimum(-1);
    hADCvsTIME[k]->Reset("ICESM");
    for (int i = 0; i < n::samples; i++)
    {
      hADCvsTIME[k]->Fill(i, 0);
    }
  }
}

/*******************************************************************************
help() to display usage
*******************************************************************************/
void help()
{
  printf("fdecoder <options>\n");
  printf("   -h                   : print usage\n");
  printf("   -i <input_file>      : input file name with a path\n");
  printf("   -o <output_path>     : output files path\n");
  printf("   -p <Value>           : number of pre-samples below threshold in zero-suppressed mode\n");
  printf("   -v <level>           : verbose\n");
  printf("   -t                   : test converter workability - run iver 30 first events\n");
  printf("   -f <0xFlags>         : flags to determine printed items\n");
  exit(1);
}

/*******************************************************************************
parse_cmd_args() to parse command line arguments
*******************************************************************************/
int parse_cmd_args(int argc, char **argv, Param *p)
{
  /*int i;
  int match = 0;
  int err = 0;*/

  for (;;) {
    int c = getopt(argc, argv, "i:o:p:ht");
    if (c < 0) break;
    switch (c) {
      case 'i' : p->inp_file          = optarg;       break;
      case 'o' : p->out_path          = optarg;       break;
      case 'v' : verbose              = atoi(optarg); break;
      case 't' : p->test              = true;         break;
      case 'f' : p->vflag                   = atoi(optarg); break;
      case 'p' : p->sample_index_offset_zs  = atoi(optarg); break;
      case '?' : help();
    }
  }
  if (argc == 1)
    help();

  return (0);
}

/*******************************************************************************
Main
*******************************************************************************/
int main(int argc, char **argv)
{
  unsigned short datum;
  int err;
  bool done;
  int ntot;

  DAQ daq;
  daq.loadDAQ();
  cout << "... DAQ loaded successfully" << endl;

  Mapping T2K;
  T2K.loadMapping();
  cout << "...Mapping loaded succesfully." << endl;

  Pads padPlane;
  padPlane.loadPadPlane(daq, T2K);
  cout << "...Pads loaded succesfully." << endl;

  // Default parameters
  //sprintf(param.inp_file, "C:\\users\\calvet\\projects\\bin\\pandax\\data\\R2018_11_27-15_24_07-000.aqs"); //Change default file path
  param.sample_index_offset_zs = 4;
  param.vflag = 0;
  param.has_no_run = 0;
  param.show_run = 0;
  verbose = 1;

  // Parse command line arguments
  if (parse_cmd_args(argc, argv, &param) < 0)
    return (-1);

  // Open input file
  string file_in = param.inp_file;

  if (!(param.fsrc = fopen(file_in.c_str(), "rb")))
  {
    printf("could not open file %s.\n", file_in.c_str());
    return (-1);
  }

  scan();
  ntot = eventPos.size();

  while (file_in.find("/") != string::npos)
    file_in = file_in.substr(file_in.find("/") + 1);
  file_in = file_in.substr(0, file_in.find("."));

  string out_file = param.out_path + file_in + ".root";

  TFile *output_file = new TFile((out_file).c_str(), "RECREATE");
  TTree *tree = new TTree("tree", "tree");

  Int_t eventNumber;
  Int_t PadAmpl[geom::nPadx][geom::nPady][n::samples] = {};

  tree->Branch("eventNumber", &eventNumber, "eventNumber/I");
  tree->Branch("PadAmpl", &PadAmpl, Form("PadAmpl[%i][%i][%i]/I", geom::nPadx, geom::nPady, n::samples));

  if (verbose) {
    printf("Decoder    : Version %d.%d Compiled %s at %s\n", dc.DecoderMajorVersion, dc.DecoderMinorVersion, dc.DecoderCompilationDate, dc.DecoderCompilationTime);
    cout << "Processing file" << endl;
    cout << "Input  : " << param.inp_file << endl;
    cout << "Output : " << out_file << endl;
  }

  // in case of test run
  if (param.test) ntot = 30;
  // loop over events
  for (int i = firstEv; i < firstEv + ntot; i++) {
    if (i % 100 == 0) {
      cout << "\r"
         << "\t"
         << "... Processing event " << i << " ..." << flush;
    }

    DatumContext_Init(&dc, param.sample_index_offset_zs);

    fseek(param.fsrc, eventPos[i - firstEv], SEEK_SET);
    // Scan the file
    done = true;

    // clean the padAmpl
    memset(PadAmpl, 0, geom::nPadx * geom::nPady * n::samples * (sizeof(Int_t)));

    while (done) {
      if (fread(&datum, sizeof(unsigned short), 1, param.fsrc) != 1) {
        done = false;
        if (ferror(param.fsrc))
          cout << "ERROR" << endl;
        if (feof(param.fsrc))
          cout << "reach EOF" << endl;
      } else {

        fea.tot_file_rd += sizeof(unsigned short);
        // Interpret datum
        if ((err = Datum_Decode(&dc, datum)) < 0) {
          printf("%d Datum_Decode: %s\n", err, &dc.ErrorString[0]);
          done = true;
        }
        // Decode
        if (dc.isItemComplete) {

          if (dc.ItemType == IT_START_OF_EVENT && (int)dc.EventNumber == i) {
            eventNumber = (int)dc.EventNumber;
          } else if (eventNumber == i && dc.ItemType == IT_ADC_SAMPLE) {
            if (dc.ChannelIndex != 15 && dc.ChannelIndex != 28 && dc.ChannelIndex != 53 && dc.ChannelIndex != 66 && dc.ChannelIndex > 2 && dc.ChannelIndex < 79) {

              int a = 0;
              int b = 0;
              // histo and display
              int x = T2K.i(dc.CardIndex, dc.ChipIndex, daq.connector(dc.ChannelIndex));
              int y = T2K.j(dc.CardIndex, dc.ChipIndex, daq.connector(dc.ChannelIndex));

              a = (int)dc.AbsoluteSampleIndex;
              b = (int)dc.AdcSample;

              PadAmpl[x][y][a] = b;
            }
          }
          else if (dc.ItemType == IT_DATA_FRAME) {}
          else if (dc.ItemType == IT_END_OF_FRAME) {}
          else if (dc.ItemType == IT_MONITORING_FRAME) {}
          else if (dc.ItemType == IT_CONFIGURATION_FRAME) {}
          else if (dc.ItemType == IT_SHORT_MESSAGE) {}
          else if (dc.ItemType == IT_LONG_MESSAGE) {}
          else if (eventNumber == i && dc.ItemType == IT_TIME_BIN_INDEX) {}
          else if (eventNumber == i && dc.ItemType == IT_CHANNEL_HIT_HEADER) {}
          else if (dc.ItemType == IT_DATA_FRAME) {}
          else if (dc.ItemType == IT_NULL_DATUM) {}
          else if (dc.ItemType == IT_CHANNEL_HIT_COUNT) {}
          else if (dc.ItemType == IT_LAST_CELL_READ) {}
          else if (dc.ItemType == IT_END_OF_EVENT) {
            // go to the next event
            done = false;
          }
          else if (dc.ItemType == IT_PED_HISTO_MD) {}
          else if (dc.ItemType == IT_UNKNOWN) {}
          else if (dc.ItemType == IT_CHAN_PED_CORRECTION) {} //printf("Type : 0x%x \n", dc.ItemType);}
          else if (dc.ItemType == IT_CHAN_ZERO_SUPPRESS_THRESHOLD) {} //printf("Type : 0x%x \n", dc.ItemType);}
          else {} //}&& dc.ItemType==IT_START_OF_EVENT
        } // end of if (dc.isItemComplete)
      } // end of second loop inside while
    } // end of while(done) loop

    // Fill in the tree
    tree->Fill();
  } // end of loop over events

  tree->Write("", TObject::kOverwrite);

  output_file->Close();

  // Close file if it has been opened
  if (param.fsrc)
    fclose(param.fsrc);

  std::cout << "Finished" << std::endl;

  return (0);
}
